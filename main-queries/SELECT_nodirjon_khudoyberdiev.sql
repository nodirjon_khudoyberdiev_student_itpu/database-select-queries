-- Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?
WITH StaffRevenue AS (
    SELECT
        s.store_id,
        s.staff_id,
        s.first_name,
        s.last_name,
        SUM(p.amount) AS total_revenue
    FROM
        staff s
    INNER JOIN
        payment p ON s.staff_id = p.staff_id
    INNER JOIN
        rental r ON p.rental_id = r.rental_id
    WHERE
        EXTRACT(YEAR FROM r.rental_date) = 2017
    GROUP BY
        s.store_id,
        s.staff_id,
        s.first_name,
        s.last_name
),
MaxRevenuePerStore AS (
    SELECT
        store_id,
        MAX(total_revenue) AS max_revenue
    FROM
        StaffRevenue
    GROUP BY
        store_id
)
SELECT
    sr.store_id,
    sr.staff_id,
    sr.first_name,
    sr.last_name,
    sr.total_revenue
FROM
    StaffRevenue sr
INNER JOIN
    MaxRevenuePerStore mrps ON sr.store_id = mrps.store_id AND sr.total_revenue = mrps.max_revenue;



--  Which five movies were rented more than the others, and what is the expected age of the audience for these movies?
WITH TopFiveMovies AS (
    SELECT
        f.film_id,
        f.title,
        COUNT(r.rental_id) AS rental_count
    FROM
        film f
    INNER JOIN
        inventory i ON f.film_id = i.film_id
    INNER JOIN
        rental r ON i.inventory_id = r.inventory_id
    GROUP BY
        f.film_id,
        f.title
    ORDER BY
        COUNT(r.rental_id) DESC
    LIMIT
        5
)
SELECT
    tm.title,
    tm.rental_count,
    AVG(DATE_PART('year', CURRENT_DATE) - DATE_PART('year', c.create_date)) AS expected_age
FROM
    TopFiveMovies tm
JOIN
    inventory i ON tm.film_id = i.film_id
JOIN
    rental r ON i.inventory_id = r.inventory_id
JOIN
    customer c ON r.customer_id = c.customer_id
GROUP BY
    tm.title,
    tm.rental_count;




--Which actors/actresses didn't act for a longer period of time than the others?
WITH ActorLastFilm AS (
    SELECT
        fa.actor_id,
        MAX(f.release_year) AS last_film_year
    FROM
        film_actor fa
    INNER JOIN
        film f ON fa.film_id = f.film_id
    GROUP BY
        fa.actor_id
),
ActorInactivePeriod AS (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        COALESCE(MAX(DATE_PART('year', CURRENT_DATE) - al.last_film_year), 0) AS inactive_years
    FROM
        actor a
    LEFT JOIN
        ActorLastFilm al ON a.actor_id = al.actor_id
    GROUP BY
        a.actor_id,
        a.first_name,
        a.last_name
)
SELECT
    actor_id,
    first_name,
    last_name,
    inactive_years
FROM
    ActorInactivePeriod
ORDER BY
    inactive_years DESC;



