--Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?
WITH StaffRevenue AS (
    SELECT
        s.store_id,
        s.staff_id,
        s.first_name,
        s.last_name,
        SUM(p.amount) AS total_revenue,
        RANK() OVER(PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS revenue_rank
    FROM
        staff s
    INNER JOIN
        payment p ON s.staff_id = p.staff_id
    INNER JOIN
        rental r ON p.rental_id = r.rental_id
    WHERE
        EXTRACT(YEAR FROM r.rental_date) = 2017
    GROUP BY
        s.store_id,
        s.staff_id,
        s.first_name,
        s.last_name
)
SELECT
    store_id,
    staff_id,
    first_name,
    last_name,
    total_revenue
FROM
    StaffRevenue
WHERE
    revenue_rank = 1;





--Which five movies were rented more than the others, and what is the expected age of the audience for these movies?
WITH TopFiveMovies AS (
    SELECT
        f.film_id,
        f.title,
        COUNT(r.rental_id) AS rental_count
    FROM
        film f
    INNER JOIN
        inventory i ON f.film_id = i.film_id
    INNER JOIN
        rental r ON i.inventory_id = r.inventory_id
    GROUP BY
        f.film_id,
        f.title
    ORDER BY
        COUNT(r.rental_id) DESC
    LIMIT
        5
),
AverageAge AS (
    SELECT
        f.film_id,
        AVG(DATE_PART('year', CURRENT_DATE) - DATE_PART('year', c.create_date)) AS expected_age
    FROM
        TopFiveMovies f
    INNER JOIN
        inventory i ON f.film_id = i.film_id
    INNER JOIN
        rental r ON i.inventory_id = r.inventory_id
    INNER JOIN
        customer c ON r.customer_id = c.customer_id
    GROUP BY
        f.film_id
)
SELECT
    f.title,
    f.rental_count,
    COALESCE(a.expected_age, 0) AS expected_age
FROM
    TopFiveMovies f
LEFT JOIN
    AverageAge a ON f.film_id = a.film_id;







--Which actors/actresses didn't act for a longer period of time than the others?
WITH ActorFilmCounts AS (
    SELECT
        fa.actor_id,
        COUNT(DISTINCT f.film_id) AS film_count
    FROM
        film_actor fa
    INNER JOIN
        film f ON fa.film_id = f.film_id
    GROUP BY
        fa.actor_id
),
MaxFilmCount AS (
    SELECT
        MAX(film_count) AS max_film_count
    FROM
        ActorFilmCounts
),
ActorInactivePeriod AS (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        COALESCE(mfc.max_film_count - afc.film_count, 0) AS inactive_years
    FROM
        actor a
    LEFT JOIN
        ActorFilmCounts afc ON a.actor_id = afc.actor_id
    CROSS JOIN
        MaxFilmCount mfc
)
SELECT
    actor_id,
    first_name,
    last_name,
    inactive_years
FROM
    ActorInactivePeriod
ORDER BY
    inactive_years DESC;
